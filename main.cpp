#include <iostream>
#include <fstream>
#include <string>
#include <filesystem>
#include <bitset>
#include <future>
#include <stack>
#include <tuple>
#include <optional>
#include <utility>

//Parameters array
bool p_BackupDir = false; //+
std::filesystem::path p_BackupDirPath; //+
bool p_BackupDirMkNew = false; //+
std::bitset<4> p_Langs = 15; //+
bool p_Dir = false; //+
std::filesystem::path p_DirPath; //+
bool p_OutputDir = false; //+
std::filesystem::path p_OutputDirPath; //+
std::bitset<3> p_RenameVariablesLangs=0;

class tagCheck{
    int i_tag=0,size=0;
    bool opened=false,in_array=false;
    std::string tags[2];
    const std::string parameters="-a-zA-Z/:.\"' _=";
    int increment_i_tag(int inc=1){
        i_tag+=inc;
        if (i_tag >= tags[opened].length()){
            i_tag=0;
            opened=!opened;
            int tempsize=size;
            size=0;
            return opened?tempsize:-tempsize;
        } else {
            return 0;
        }
    }
public:
    tagCheck(std::string i_opentag,std::string i_closetag){
        tags[0]=i_opentag;
        tags[1]=i_closetag;
    }
    //Opened - returns size_of_tag, otherwise -size_of_tag
    int check(const char &i_Ch){
        // < pre >     < script*>
        //* = parameters in tag. As a src="example.example/dir/example.js"
        if (tags[opened][i_tag] != ' ' && tags[opened][i_tag] != '*'){
            if (tags[opened][i_tag] == i_Ch){
                size++;
                return increment_i_tag();
            } else {
                size=0;
                i_tag=0;
                return 0;
            }
        }
        if (tags[opened][i_tag] == ' '){
            if (i_Ch == ' '){
                size++;
                return 0;
            }
            if (tags[opened][i_tag+1] == i_Ch){
                size++;
                return increment_i_tag(2);
            } else {
                size=0;
                i_tag=0;
                return 0;
            }
        }
        if (tags[opened][i_tag] == '*'){
            bool exist=false;
            for(int unsigned i = 0; i<parameters.length();i++){
                if (parameters[i] != '-'){
                    if (parameters[i] == i_Ch){
                        exist=true;
                        break;
                    }
                } else {
                    if (i>0 && i <parameters.length()-1){
                        for (char j = parameters[i-1]; j != parameters[i+1]; j++){
                            if (i_Ch == j){
                                exist=true;
                                break;
                            }
                        }
                        if (exist){
                            break;
                        }
                    } else {
                        if (i_Ch == '-'){
                            exist=true;
                            break;
                        }
                    }
                }
            }
            if (exist){
                size++;
            } else {
                if (tags[opened][i_tag+1] == ' ' && i_Ch == ' '){
                    size++;
                    return increment_i_tag();
                }
                if (tags[opened][i_tag+1] == ' ' && i_Ch == tags[opened][i_tag+2]){
                    size++;
                    return increment_i_tag(3);
                }
                if (i_Ch == tags[opened][i_tag+1]){
                    size++;
                    return increment_i_tag(2);
                }
                //else
                size=0;
                i_tag=0;
                return 0;
            }
        }
    }
};

std::tuple<bool,std::optional<std::filesystem::path>,std::optional<const std::exception*>> fileprocess(std::filesystem::path path){
    try{
        //File processing
        if (std::filesystem::is_regular_file(path)){
            std::fstream file(path,std::ios::in|std::ios::binary);
            std::string out;
            char Ch;
            tagCheck tags[]={tagCheck("< pre >","</ pre >"),tagCheck("< script*>","</ script >"),tagCheck("<?php","?>")};

            //tags
            bool in_css=path.extension()==".css",maybe_clear_line_break=false,maybe_clear_space=false;
            int in_pre=0,in_js=path.extension()==".js",in_php=0,tag_check_result;
            int unsigned tag_ignore_lengths[3] = {0,0,0};

            while (file.get(Ch)){
                //pre tag check
                tag_check_result=tags[0].check(Ch);
                if (tag_check_result){
                    if (tag_check_result > 0){
                        in_pre++;
                    } else if (in_php) {
                        in_pre--;
                    }
                }
                tag_check_result=tags[1].check(Ch);
                if (tag_check_result){
                    if (tag_check_result > 0){
                        in_js++;
                    } else if (in_js) {
                        in_js--;
                    }
                }
                tag_check_result=tags[2].check(Ch);
                if (tag_check_result){
                    if (tag_check_result > 0){
                        in_php++;
                    } else if (in_php) {
                        in_php--;
                    }
                }

                if (in_css){

                } else if (in_php){

                } else if (in_js){

                } else {
                    //html
                    if (!in_pre){
                        if (Ch == '\n' || Ch=='\r'){
                            maybe_clear_line_break = true;
                            continue;
                        } else if (maybe_clear_line_break){
                            if (out.back() != '<' && out.back() != '>' && out.back() != '/' && Ch != ' '){
                                out+=' ';
                            }
                            maybe_clear_line_break=false;
                        }
                        if (Ch == ' '){
                            maybe_clear_space=true;
                            continue;
                        } else if (maybe_clear_space){
                            if (out.back() != '<' && out.back() != '>' && out.back() != '/' && Ch != '<' && Ch != '>' && Ch != '/'){
                                out+=' ';
                            }
                            maybe_clear_space=false;
                        }
                    }
                }

                //file processing
                out+=Ch;
            }
            file.close();

            if (!p_OutputDir){
                file.open(path,std::ios::out|std::ios::binary|std::ios::trunc);
            } else {
                file.open(p_OutputDirPath/path.lexically_relative(p_DirPath),std::ios::out|std::ios::binary|std::ios::trunc);
            }
            file << out;
            file.close();
        }
    } catch(const std::exception& Err){
        return std::make_tuple(false,path,&Err);
    }
    return std::make_tuple(true,std::nullopt,std::nullopt);
}

int main(int argc, char* argv[]) {
    std::locale::global(std::locale(""));
    //Parameters check
    if (argc == 1){
        std::cout << "Add -h or --help parameter to get help\n";
        return 0;
    }

    for(int i = 1; i < argc; i++){
        std::string param(argv[i]);
        if (param == "-h" || param == "--help"){
            std::cout << "Use: lum [OPTIONS] [DIR]\nParameters:\n-b [DIR]\tspecify backup directory\n-bmk\tcreate new directory in the -b path\n-l [LANGS]\tspecify languages to process(-l html,php,js,css)\n-o [DIR]\tspecify output directory\n-rv [LANGS]\tenable variables(classes) renaming feature for specified languages(-rv html+css,php,js)\n";
            return 0;
        } else if (param == "-b"){
            //Check if exists next parameter
            if (i == argc-1){
                std::cout << "-b parameter requires specifier (-b [DIR])\n";
                return 0;
            } else {
                i++;
                if (!std::filesystem::exists(argv[i])){
                    //Not exist
                    std::cout << argv[i] << " dir isn`t exist\n";
                    return 0;
                } else {
                    if (!std::filesystem::is_directory(argv[i])){
                        std::cout << argv[i] << " isn`t dir\n";
                        return 0;
                    } else {
                        p_BackupDir=true;
                        p_BackupDirPath=argv[i];
                    }
                }
            }
        } else if (param == "-o"){
            //Check if exists next parameter
            if (i == argc-1){
                std::cout << "-o parameter requires specifier (-o [DIR])\n";
                return 0;
            } else {
                i++;
                if (!std::filesystem::exists(argv[i])){
                    //Not exist
                    std::cout << argv[i] << " dir isn`t exist\n";
                    return 0;
                } else {
                    if (!std::filesystem::is_directory(argv[i])){
                        std::cout << argv[i] << " isn`t dir\n";
                        return 0;
                    } else {
                        p_OutputDir = true;
                        p_OutputDirPath = argv[i];
                    }
                }
            }
        } else if (param == "-bmk"){
            p_BackupDirMkNew = true;
        } else if (param == "-l"){
            if (i == argc-1){
                std::cout << "-l parameter requires specifier (-l [LANGS])\n";
                return 0;
            } else {
                i++;
                std::istringstream langsis(argv[i]);
                for (std::string temp; std::getline(langsis, temp, ',');) {
                    if (temp == "html") {
                        p_Langs[0] = true;
                    } else if (temp == "php") {
                        p_Langs[1] = true;
                    } else if (temp == "js") {
                        p_Langs[2] = true;
                    } else if (temp == "css") {
                        p_Langs[3] = true;
                    } else {
                        std::cout << temp << " is wrong language\n";
                        return 0;
                    }
                }
            }
        } else if (param == "-rv"){
            if (i == argc-1){
                std::cout << "-rv parameter requires specifier (-rv [LANGS])\n";
                return 0;
            } else {
                i++;
                std::istringstream langsis(argv[i]);
                for (std::string temp; std::getline(langsis, temp, ',');) {
                    if (temp == "html+css") {
                        p_RenameVariablesLangs[0] = true;
                    } else if (temp == "php") {
                        p_RenameVariablesLangs[1] = true;
                    } else if (temp == "js") {
                        p_RenameVariablesLangs[2] = true;
                    } else {
                        std::cout << temp << " is wrong language\n";
                        return 0;
                    }
                }
            }
        } else if (i==argc-1){
            if (!std::filesystem::exists(argv[i])){
                //Not exist
                std::cout << argv[i] << " dir isn`t exist\n";
                return 0;
            } else {
                if (!std::filesystem::is_directory(argv[i])){
                    std::cout << argv[i] << " isn`t dir\n";
                    return 0;
                } else {
                    p_Dir = true;
                    p_DirPath = argv[i];
                }
            }
        } else {
            std::cout << "Unknown parameter: " << param << "\n";
            return 0;
        }
    }
    //Parameters check end, dir check
    if (!p_Dir){
        std::cout << "Work directory not specified\n";
        return 0;
    } else if (!std::filesystem::is_directory(p_DirPath)){
            std::cout << p_DirPath << " isn`t dir\n";
            return 0;
    }

    if (p_BackupDir){
        try{
            if (!std::filesystem::exists(p_BackupDirPath)){
                std::cout << "Backup dir created\n";
                std::filesystem::create_directory(p_BackupDirPath);
            }
            if (p_BackupDirMkNew){
                time_t BackupTime = time(nullptr);
                std::filesystem::create_directory(p_BackupDirPath.append(std::string("lum ")+std::ctime(&BackupTime)));
            }
            std::filesystem::copy(p_DirPath,p_BackupDirPath,std::filesystem::copy_options::recursive);
            std::cout << "Backup created\n";
        } catch(const std::exception& Err){
            std::cout << "Backup error: " << Err.what() << "\n";
            return 0;
        }
    }

    std::stack<std::future<std::tuple<bool,std::optional<std::filesystem::path>,std::optional<const std::exception*>>>> AsyncStack;
    std::stack<std::pair<std::filesystem::path,const std::exception*>> ErrorStack;
    unsigned int AsyncCount=0,AsyncProcessed=0,ErrorCount=0;

    std::cout << "Objects to process: 0\n";
    for (auto& file: std::filesystem::recursive_directory_iterator(p_DirPath)){
        AsyncStack.push(std::async(fileprocess,file.path()));
        AsyncCount++;
        std::cout << "\033[1A\033[KObjects to process: "<<AsyncCount<<"\n";
    }

    std::cout << "Objects processed: 0(0%)\n";
    while (!AsyncStack.empty()){
        AsyncStack.top().wait();
        std::tuple<bool,std::optional<std::filesystem::path>,std::optional<const std::exception*>> AsyncRes = AsyncStack.top().get();
        AsyncStack.pop();
        if (!std::get<0>(AsyncRes)){
            ErrorStack.push(std::make_pair(std::get<1>(AsyncRes).value(),std::get<2>(AsyncRes).value()));
            ErrorCount++;
        }
        AsyncProcessed++;
        std::cout << "\033[1A\033[KObjects processed: " << AsyncProcessed << "(" << AsyncProcessed*100/AsyncCount << "%)";
        if (ErrorCount){
            std::cout << ", errors: "<<ErrorCount<<"(" << ErrorCount*100/AsyncCount << "%)\n";
        } else {
            std::cout << "\n";
        }
    }

    while(!ErrorStack.empty()){
        //Error checking
        std::cout << (*ErrorStack.top().second).what() << " - " << ErrorStack.top().first << "\n";
        ErrorStack.pop();
    }

    return 0;
}